Use the `install.sh` script to download, compile, and install [MakeMKV](www.makemkv.com).

# What the script does

MakeMKV is distributed as 2 tarballs: one containing open source code and one containing a binary blob. The script downloads and extracts both tarballs, compiles the open source component, and then installs both components to a directory of your choice.

The binary component contains a poorly-written Makefile that doesn't let you easily customize the installation directory. The script patches the Makefile to match the requested installation directory.

# Prerequisites

The script uses `curl` to download the tarballs.

Compiling MakeMKV requires several packages. The [download page](https://www.makemkv.com/forum/viewtopic.php?f=3&t=224) will have the most up-to-date list.

```
sudo apt install \
    curl \
    build-essential \
    pkg-config \
    libc6-dev \
    libssl-dev \
    libexpat1-dev \
    libavcodec-dev \
    libgl1-mesa-dev \
    qtbase5-dev \
    zlib1g-dev
```

# Running the script

The script requires 2 arguments: the MakeMKV version and the directory where it will be installed. For example, run the script as:

```
./install.sh 1.12.3 /usr/local/stow/makemkv-1.12.3
```

to install version `1.12.3` to the directory `/usr/local/stow/makemkv-1.12.3`.

Some steps in the scripts use `sudo`, so you may be asked for your password. One step in the script requires you to accept the EULA.

# After running the script

If you installed to a location managed by [stow](https://www.gnu.org/software/stow/), you'll need to run a command such as the following to unstow the old version and then stow the new version:

```
sudo stow -d /usr/local/stow \
    -D makemkv-1.12.2 \
    -S makemkv-1.12.3
```

Furthermore, you may need to update the cache of shared libraries with:

```
sudo ldconfig
```

MakeMKV is free while it's in beta, but you will need a registration key. The key is updated every couple of months and is posted at https://www.makemkv.com/forum/viewtopic.php?f=5&t=1053. Search for "MakeMKV is free while in beta" if this link is broken.

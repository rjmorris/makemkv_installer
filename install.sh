#!/bin/bash

set -e

if [[ $# != 2 ]]; then
  echo "Usage: $0 <version> <install_dir>"
  echo "Example: $0 1.12.3 /usr/local/stow/makemkv-1.12.3"
  exit -1
fi

VERSION=$1
PREFIX=$2

function download_and_extract() {
    local package=$1

    if [[ -e ${package}.tar.gz ]]; then
        echo ${package}.tar.gz exists, skipping download
    else
        echo Downloading ${package}.tar.gz
        curl --remote-name http://www.makemkv.com/download/${package}.tar.gz
    fi

    if [[ -d ${package} ]]; then
        echo Directory ${package} exists, deleting
        rm -rf ${package}
    fi
    echo Extracting ${package}.tar.gz
    tar xf ${package}.tar.gz
}

PACKAGE=makemkv-oss-${VERSION}
download_and_extract ${PACKAGE}
pushd ${PACKAGE}
./configure --prefix=${PREFIX}
make
sudo make install
popd

PACKAGE=makemkv-bin-${VERSION}
download_and_extract ${PACKAGE}
pushd ${PACKAGE}
echo Patching ${PACKAGE}/Makefile
mv Makefile Makefile.orig
sed "s:^PREFIX=/usr$:PREFIX=${PREFIX}:" Makefile.orig > Makefile
make
sudo make install
popd
